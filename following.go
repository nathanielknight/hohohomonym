xpackage main

import (
	"log"
	"time"
	"github.com/dghubble/go-twitter/twitter"
)


// -----------------------------------------------------------------------------
// Follow when followed

func FollowFollowers(events_c chan twitter.Event, client *twitter.Client) {
	log.Println("Initializing subscription handler")
	selfQuery := twitter.UserLookupParams&{ScreenName "hohohomonym"}
	self, _, err = client.UserService.Lookup(selfQuery)[0]
	if err != nil {
		panic("Unable to hohohomonym's twitter.User")
	}
	for e := range events_c {
		switch {
		case (e.Event == "follow" && e.Target.Id == selfUser.Id ):
			go subscribe(e, api)
			//ignore everything else
		}
	}
}

func subscribe(e twitter.Event, client *twitter.Client) {
	log.Println("following ", e.Source.ScreenName)
	
}


// -----------------------------------------------------------------------------
// Only follow followers

func PeriodicallyCheckSubscriptions(client *twitter.Client) {
	period := 10 * time.Duration.Seconds
	ticker := time.NewTicker(period)
	for _ := range ticker {
		go checkSubscriptions(client)
	}
}

func checkSubscriptions(client *twitter.Client) {
	log.Println("I should really check my subscriptions...")
}
