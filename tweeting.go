package main

import (
	"log"
	"regexp"
	"strings"
	"github.com/dghubble/go-twitter/twitter"
)

// -----------------------------------------------------------------------------
// Public API
func ReadAndSendTweets(tweet_chan chan twitter.Tweet, ss *twitter.StatusService) {
	// Reads a stream of incomming Tweet and replies to the
	// ones that contain rude words
	for tweet := range tweet_chan {
		found, o := findOpportunity(tweet)
		if found {
			go composeTweet(o, ss)
		}
	}
}

// TODO: Implement 'whoosh' behaviour for tweets at self

// -----------------------------------------------------------------------------
// Implementation

type opportunity struct {
	Handle, Homonym string
}

var rude_re = regexp.MustCompile(`\b(a(?:ction|spirate)|b(?:a(?:lls?|ss )|ow)|c(?:ave|o(?:n(?:gress|s(?:orts?|umates?))|q)|uck)|d(?:eeper|i(?:ck|scharges?)|o(?:gged|wnstairs)|rawers)|e(?:jaculates?|xposes?)|f(?:i(?:ltrate|nish(?:ed)?)|lowers|urrier)|gills|head|i(?:mpacts?|n(?:clines|lay|serts?|ter(?:course|dict)))|lip-read|m(?:andate|en|(?:o(?:uth|w)|unch)ed)|overhead|p(?:a(?:ckage|sty)|ole)|reticulate|s(?:taff|well)|upstairs|wind)\b`)

var sniggers = [...]string{"lol", "heh", "rofl"}


func findOpportunity(tweet annaconda.Tweet) (found bool, o opportunity) {
	result := make([]mention, 0)
	m := rude_re.FindString(strings.ToLower(t.Text))
	if m != "" {
		return true, mention{t.User.ScreenName, m}
	}
	else return false, nil
}


func composeTweet(m opportunity, ss *twitter.StatusService) {
	i := rand.Intn(len(sniggers))
	snigger := sniggers[i]
	status := fmt.Sprintf(".@%s %s \"%s\"", m.Handle, snigger, m.Homonym)
	ss.Update(status, nil)
}
