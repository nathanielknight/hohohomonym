package main

import (
	"encoding/json"
	"io/ioutil"
	"log"	
	"os"
	"github.com/dghubble/go-twitter/twitter"
	"github.com/dghubble/oauth1"
)

type AuthTokens struct {
	ConsumerKey, ConsumerSecret, AccessKey, AccessSecret string
}


func getAuthTokens() AuthTokens {
	if len(os.Args) < 2 {
		panic("Usage: hohohomonym <config file>")
	}
	configFileName := os.Args[1]
	configData, err := ioutil.ReadFile(configFileName)
	if err != nil {
		log.Fatalln(err)
	}

	var tokens AuthTokens
	err = json.Unmarshal(configData, &tokens)
	if err != nil {
		log.Fatalln(err)
	}
	log.Println("Loaded auth tokens from ", configFileName)
	return tokens
}


func main() {
	// Load API tokens & Initialize Client
	tkns := getAuthTokens()
	config := oauth1.NewConfig(tkns.ConsumerKey, tkns.ConsumerSecret)
	token := oauth1.NewToken(tkns.AccessKey, tkns.AccessSecret)
	httpClient := config.Client(oauth1.NoContext, token)
	client := twitter.NewClient(httpClient)
	log.Println("Initialized twitter.Client")


	go PeriodicallyCheckSubscriptions

	// Subsribe to bot's User Stream
	s, err := client.Streams.User(nil)
	if err != nil {
		log.Fatalln(err)
	}
	defer s.Stop()

	// Listen to User's Stream
	for x := range s.Messages {
		log.Println(x)
	}

}
